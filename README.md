# READ ME #

## GravaJo class(es) ##

The GravaJo (classes) are two different classes for obtaining the [Gravatar]( https://gravatar.com ) Images of a given individual.  One class is for the new Xojo framework and one for the old framework.


### testing ###
currently there is no testing.

### optimization ###
currently the methods are not optimized.  All methods are using the first try to complete the method over using the best method.  Optimizations will come in the future.

### sample code ###
there is a sample desktop application that will pull down an image from Gravatar using either the old or new framework (selectable on the window).  This has has almost no error checking in it.

### version history ###
* 2018-02-14 - initial commit.
